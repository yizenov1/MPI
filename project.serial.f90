  ! This project is called Multiple Graph Sketching

  use mpi
  implicit none

  ! MPI declarations
  integer::p,rank,request(4),ierror
  integer::status(mpi_status_size),array_of_statuses(mpi_status_size,4)
  integer,parameter::dp=mpi_double_precision,comm=mpi_comm_world,tag=1
  integer,parameter::charac=mpi_char,integ=mpi_integer

  ! Timing declarations
  double precision::t0,t1,time,flush(3*1024*1024)

  ! I/O PARAMETERS
  character(LEN=3),parameter::file_name*40='project/data/graph_shingle_frequency.txt',status_val='old'
  integer,parameter::unit_number=11,hash_function_number=10,shingle_size=25

  ! GRAPH PARAMETERS
  integer::graph_number,shingle_number,shingle_index,graph_index
  integer::i,j,k,hash_value,column_sum

  character(LEN=shingle_size),allocatable::shingle_vector(:,:)
  character(LEN=shingle_size)::shingle

  integer,allocatable::frequency_vector(:,:),hashed_vector(:,:)
  integer::sketch(hash_function_number,1)
  real::hash_random(shingle_size,hash_function_number)

  call mpi_init(ierror)
  call mpi_comm_size(comm,p,ierror)
  call mpi_comm_rank(comm,rank,ierror)
  
  call random_number(flush)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  if(rank==0) then

!    print*,''
!    print*,'Multiple Graphs Sketching'
!    print*,''

    time = 0.d0
    call random_number(hash_random)

    ! Data distribution
!    print*,'Data loading'
!    print*,''

    open(unit=unit_number,file=file_name,status=status_val)
    read(unit=unit_number,fmt=*) graph_number

    do graph_index = 1, graph_number

       t0 = mpi_wtime()
      
!      print*,'graph ',graph_index

      read(unit=unit_number,fmt=*) shingle_number

      allocate(shingle_vector(shingle_number,1),frequency_vector(shingle_number,1))
      allocate(hashed_vector(shingle_number,hash_function_number))

      do shingle_index = 1, shingle_number   
        read(unit=unit_number,fmt=*) shingle_vector(shingle_index,1),frequency_vector(shingle_index,1)
      enddo

      ! Hashing computation and frequency multiplication
      do i = 1, shingle_number
        do j = 1, hash_function_number
          hash_value = 0
          shingle = shingle_vector(i,1)
          do k = 1, shingle_size
            hash_value = hash_value + ichar(shingle(k:k)) * int(hash_random(k,j))
          enddo
          hash_value = 2 * mod(hash_value, 2) - 1
          hashed_vector(i,j) = hash_value * frequency_vector(i,1)
        enddo
      enddo

      ! Summing into sketch
      do i = 1, hash_function_number
        column_sum = 0
        do j = 1, shingle_number
          column_sum = column_sum + hashed_vector(j,i)
        enddo
        sketch(i,1) = column_sum
      enddo

      ! Converting to binary vectors
      do i = 1, hash_function_number
        if(sketch(i,1) >= 0) then
          sketch(i,1) = 1
        else
          sketch(i,1) = -1
        endif
      enddo

      ! Output values
!      print*,''
!      print*,'sketch: ',sketch

      deallocate(shingle_vector,frequency_vector,hashed_vector)

      t1 = mpi_wtime() - t0
      time = time + t1
  
    enddo

    close(unit_number)

    print*,''
    print*,'p=',p
    print*,'total time=',time
    print*,'average time per graph',time / dble(graph_number)
    print*,'Graph Sketching is DONE'
    print*,''

  endif


!   p=          16
! total time=  3.355860710144043E-002
! average time per graph  5.593101183573405E-005
! Graph Sketching is DONE


! p=          32
! total time=  2.863216400146484E-002
! average time per graph  4.772027333577474E-005
! Graph Sketching is DONE


! p=          64
! total time=  2.832865715026855E-002
! average time per graph  4.721442858378092E-005
! Graph Sketching is DONE


  call mpi_finalize(ierror)
  end
