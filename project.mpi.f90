  ! This project is called Multiple Graph Sketching

  use mpi
  implicit none

  ! MPI declarations
  integer::p,rank,request(4),ierror
  integer::status(mpi_status_size),array_of_statuses(mpi_status_size,4)
  integer,parameter::dp=mpi_double_precision,comm=mpi_comm_world,tag=1
  integer,parameter::charac=mpi_char,integ=mpi_integer

  ! Timing declarations
  double precision::t0,t1,time,max_time,main_time,flush(3*1024*1024)

  ! I/O PARAMETERS
  character(LEN=3),parameter::file_name*40='project/data/graph_shingle_frequency.txt',status_val='old'
  integer,parameter::unit_number=11,hash_function_number=10,shingle_size=25

  ! GRAPH PARAMETERS
  integer::graph_number,shingle_number,shingle_index,graph_index,chunk_size
  integer::i,j,k,t,rest,hash_value,column_sum
  integer,parameter::message=1,buffer_size=4

  character(LEN=shingle_size),allocatable::shingle_vector(:,:),global_shingle_vector(:,:)
  character(LEN=shingle_size)::shingle,buffer_shingle
  character(LEN=1)::array_shingle(shingle_size,1)
  integer::temp_array_shingle(shingle_size,1)

  integer,allocatable::frequency_vector(:,:),hashed_vector(:,:)
  integer::global_sketch(hash_function_number,1),sketch(hash_function_number,1),hash_random(shingle_size,hash_function_number)
  real::temp_hash_random(shingle_size,hash_function_number)

  call mpi_init(ierror)
  call mpi_comm_size(comm,p,ierror)
  call mpi_comm_rank(comm,rank,ierror)

  call random_number(flush)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  if(rank==0) then

!    print*,''
!    print*,'Multiple Graphs Sketching'
!    print*,''

    time = 0.d0
    main_time = 0.d0
    call random_number(temp_hash_random) 

    !dir$ simd
    do i = 1, hash_function_number
      do j = 1, shingle_size, 8
        hash_random(j,i) = int(temp_hash_random(j,i))
      enddo
    enddo

    ! Data distribution
!    print*,'Data loading'
!    print*,''

    open(unit=unit_number,file=file_name,status=status_val)
    read(unit=unit_number,fmt=*) graph_number

    t0 = mpi_wtime()

    do i = 1, p-1
      call mpi_send(graph_number,1,integ,i,tag,comm,ierror)
    enddo

    do i = 1, p-1
      do j = 1, hash_function_number
        call mpi_send(hash_random(1,j),shingle_size,integ,i,tag,comm,ierror)
      enddo
    enddo

    t1 = mpi_wtime() - t0
    main_time = main_time + t1

  else

    time = 0.d0

    call mpi_recv(graph_number,1,integ,0,tag,comm,status,ierror)

    do j = 1, hash_function_number
      call mpi_recv(hash_random(1,j),shingle_size,integ,0,tag,comm,status,ierror)
    enddo

  endif

  call mpi_barrier(comm,ierror)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  do graph_index = 1, graph_number

!    flush = flush + 1.d0

    if(rank == 0) then

      t0 = mpi_wtime()
      
!      print*,'graph ',graph_index

      ! Reading a graph
      read(unit=unit_number,fmt=*) shingle_number
      allocate(shingle_vector(shingle_number,1),frequency_vector(shingle_number,1))

      do shingle_index = 1, shingle_number   
        read(unit=unit_number,fmt=*) shingle_vector(shingle_index,1),frequency_vector(shingle_index,1)
      enddo

      ! Dividing into chunks
      rest = mod(shingle_number,p)
      chunk_size = shingle_number / p + 1

      do i = 1, p-1
        call mpi_send(chunk_size,1,integ,i,tag,comm,ierror)
      enddo

      ! Sending chunks
      j = 1
      do i = 1, p-1

        k = j        
        do while (k < j+chunk_size)

          shingle = shingle_vector(k,1)
          !dir$ simd
          do t = 1, shingle_size, 8
            array_shingle(t,1) = shingle(t:t)
          enddo
          
          ! Buffered sending TODO: use persistent send
          t = 1
          do while(t < shingle_size)
            call mpi_send(array_shingle(t,1),buffer_size,charac,i,tag,comm,ierror)
            t = t + buffer_size
          enddo

          call mpi_send(frequency_vector(k,1),1,integ,i,tag,comm,ierror)

          k = k + 1
        enddo

        j = j + chunk_size
      enddo
        
      deallocate(shingle_vector,frequency_vector)

      t1 = mpi_wtime() - t0
      main_time = main_time + t1

    else

      t0 = mpi_wtime()

      call mpi_recv(chunk_size,1,integ,0,tag,comm,status,ierror)

      allocate(frequency_vector(chunk_size,1), hashed_vector(chunk_size,hash_function_number))

      ! Receiving
      do i = 1, chunk_size

        ! Buffered receiving
        j = 1
        do while (j < shingle_size) !TODO: receives extra data
          call mpi_recv(array_shingle(j,1),buffer_size,charac,0,tag,comm,status,ierror)
          j = j + buffer_size
        enddo

        !dir$ simd
        do k = 1, shingle_size
          temp_array_shingle(k,1) = ichar(array_shingle(k,1))
        enddo

        call mpi_recv(frequency_vector(i,1),1,integ,0,tag,comm,status,ierror)
        
        ! Hashing computation and frequency multiplication
        do j = 1, hash_function_number
          hash_value = 0
          do k = 1, shingle_size
            hash_value = hash_value + temp_array_shingle(k,1) * hash_random(k,j)
          enddo
          hashed_vector(i,j) = (2 * mod(hash_value, 2) - 1) * frequency_vector(i,1)
        enddo
        
      enddo

      ! Summing into sketch
      do i = 1, hash_function_number
        sketch(i,1) = sum(hashed_vector(:,i))
      enddo

      deallocate(frequency_vector,hashed_vector)

      t1 = mpi_wtime() - t0
      time = time + t1

    endif

    call mpi_barrier(comm,ierror)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    if(rank == 0) then

      t0 = mpi_wtime()
      
      !dir$ simd
      do i = 1, hash_function_number
        global_sketch(i,1) = 0
      enddo

      ! Receiving local sums and global summing
      do i = 1, p-1
        call mpi_recv(sketch,hash_function_number,integ,i,tag,comm,status,ierror)
        do j = 1, hash_function_number
          global_sketch(j,1) = global_sketch(j,1) + sketch(j,1)
        enddo
      enddo

      ! Converting to binary vectors
      do i = 1, hash_function_number
        if(global_sketch(i,1) >= 0) then
          global_sketch(i,1) = 1
        else
          global_sketch(i,1) = -1
        endif
      enddo

      t1 = mpi_wtime() - t0
      main_time = main_time + t1

      ! Output values
!      print*,''
!      print*,'sketch: ',global_sketch

    else

      t0 = mpi_wtime()

      ! Sending back
      call mpi_send(sketch,hash_function_number,integ,0,tag,comm,ierror)        

      t1 = mpi_wtime() - t0
      time = time + t1

    endif

    call mpi_barrier(comm,ierror)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    

  enddo

  call mpi_reduce(time,max_time,1,dp,mpi_max,0,comm,ierror)

  if(rank==0) then
    print*,''
    print*,'p=',p
    print*,'total time=',main_time+max_time
    print*,'average time per graph=',max_time / dble(graph_number)
    print*,'Graph Sketching is DONE'
    print*,''
    close(unit_number)
  endif


! p=          16
! total time=  0.215482234954834
! average time per graph=  1.790678501129151E-004
! Graph Sketching is DONE


! p=          32
! total time=   1.84078598022461
! average time per graph=  1.525309880574544E-003
! Graph Sketching is DONE


! p=          64
! total time=   1.75622844696045
! average time per graph=  1.915723085403442E-003
! Graph Sketching is DONE


  call mpi_finalize(ierror)
  end
